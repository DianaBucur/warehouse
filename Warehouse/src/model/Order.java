package model;

public class Order {
	
	private int id;
	private String cname;
	private String pname;
	private int amount;
	
	/**
	 * Contructor for Order objects.
	 * @param id order if 
	 * @param cname name of the client who makes the order
	 * @param pname name of the product ordered
	 * @param amount quantity of the product ordered
	 */
	public Order(int id, String cname, String pname, int amount){
		this.id = id;
		this.cname = cname;
		this.pname = pname;
		this.amount = amount;
	}
	
	public Order(String cname, String pname, int amount){
		this.cname = cname;
		this.pname = pname;
		this.amount = amount;
	}

	public int getId() {
		return id;
	}

	public String getCname() {
		return cname;
	}

	
	public String getPname() {
		return pname;
	}


	public int getAmount() {
		return amount;
	}

	
	
}
