package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.table.DefaultTableModel;

import connection.ConnectionFactory;
import model.Client;
import net.proteanit.sql.DbUtils;
import presentation.Controller;

public class ClientDAO{

	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	
	private static final String insertStatementString = "INSERT INTO client (name,address,email)" + " VALUES (?,?,?)";
	private final static String findStatementString = "SELECT * FROM client WHERE name = ?";
	private final static String viewStatementString = "SELECT * FROM client";
	private final static String deleteStatementString = "DELETE FROM client WHERE id = ?";
	private final static String updateStatementString = "UPDATE client SET name = ?, address = ?, email = ? WHERE id = ?";

	/**
	 * 
	 * @param name name of the client selected in combobox that needs to be searched for in the database in order to use its data
	 * @return an Client object with the name selected
	 */
	public static Client findByName(String name) {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null; Client client = null;
		try {
			findStatement = connection.prepareStatement(findStatementString);
			findStatement.setString(1, name);
			rs = findStatement.executeQuery();
			rs.next();

			int id = rs.getInt(1);
			String name2 = rs.getString(2);
			String address = rs.getString(3);
			String email = rs.getString(4);
			client = new Client(id, name2, address, email);
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(connection);
		}
		return client;
	}

	/**
	 * 
	 * @param client client to be inserted in the database
	 * @return the index in which it was introduced
	 */
	public static int insert(Client client) {
		Connection connection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = connection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, client.getName());
			insertStatement.setString(2, client.getAddress());
			insertStatement.setString(3, client.getEmail());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(connection);
		}
		return insertedId;
	}
	
	/**
	 * 
	 * @return the data to be inserted in the table
	 * @throws SQLException
	 */
	public static DefaultTableModel buildTableModel() throws SQLException {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement viewStatement = null;
		ResultSet rs = null;
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		Vector<String> columnNames = null;
		try {
			viewStatement = connection.prepareStatement(viewStatementString);
			rs = viewStatement.executeQuery();
			ResultSetMetaData metaData = rs.getMetaData();
			columnNames = new Vector<String>();
		    int columnCount = metaData.getColumnCount();
		    for (int column = 1; column <= columnCount; column++) {
		        columnNames.add(metaData.getColumnName(column));
		    }

		    // data of the table
		    while (rs.next()) {
		        Vector<Object> vector = new Vector<Object>();
		        for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
		            vector.add(rs.getObject(columnIndex));
		        }
		        data.add(vector);
		    }
		} catch(Exception e){
			
		}
	    
	    return new DefaultTableModel(data, columnNames);

	}
	
	public static void fillComboBox(){
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement viewStatement = null;
		ResultSet rs = null;
		try {
			viewStatement = connection.prepareStatement(viewStatementString);
			rs = viewStatement.executeQuery();
			while(rs.next()){
				Controller.clientbox.addItem(rs.getString(2));
			}
		}catch(SQLException e){
		}finally {
			ConnectionFactory.close(viewStatement);
			ConnectionFactory.close(connection);
		}
	}
	
	/**
	 * 
	 * @param id the client with this id must be deleted
	 */
	public static void delete(int id){
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = connection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1, id);
			System.out.println(deleteStatement);
			deleteStatement.execute();
			
		}catch(SQLException e){
			LOGGER.log(Level.WARNING, "ClientDAO:delete " + e.getMessage());
		}finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(connection);
		}
	}
	
	/**
	 * 
	 * @param id the index where the update must take place
	 * @param client the data from this client must be added in the database at index id 
	 */
	public static void update(int id, Client client){
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		try {
			updateStatement = connection.prepareStatement(updateStatementString);
			updateStatement.setString(1, client.getName());
			updateStatement.setString(2, client.getAddress());
			updateStatement.setString(3, client.getEmail());
			updateStatement.setInt(4, id);
			updateStatement.execute();
			
		}catch(SQLException e){
			LOGGER.log(Level.WARNING, "ClientDAO:delete " + e.getMessage());
		}finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(connection);
		}
	}
}
