package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.table.DefaultTableModel;

import connection.ConnectionFactory;
import model.Client;
import model.Product;
import net.proteanit.sql.DbUtils;
import presentation.Controller;

public class ProductDAO{

protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	
	private static final String insertStatementString = "INSERT INTO product (name,amount,price)" + " VALUES (?,?,?)";
	private final static String findStatementString = "SELECT * FROM product WHERE name = ?";
	private final static String viewStatementString = "SELECT * FROM product";
	private final static String deleteStatementString = "DELETE FROM product WHERE id = ?";
	private final static String updateStatementString = "UPDATE product SET name = ?, amount = ?, price = ? WHERE id = ?";

	/**
	 * Selects the product with the name given. 
	 * @param name the name by which we search the product object
	 * @return the product object with name given
	 */
	public static Product findByName(String name) {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null; Product p = null;
		try {
			findStatement = connection.prepareStatement(findStatementString);
			findStatement.setString(1, name);
			rs = findStatement.executeQuery();
			rs.next();

			int id = rs.getInt(1);
			String name2 = rs.getString(2);
			int amount = rs.getInt(3);
			double price = rs.getDouble(4);
			p = new Product(id, name2, amount, price);
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(connection);
		}
		return p;
	}

	/**
	 * Insert a product in the database.
	 * @param p product object whose data must be inserted
	 * @return the index in which it was inserted
	 */
	public static int insert(Product p) {
		Connection connection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = connection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, p.getName());
			insertStatement.setInt(2, p.getAmount());
			insertStatement.setDouble(3, p.getPrice());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(connection);
		}
		return insertedId;
	}
	
	/**
	 * 
	 * @return the data to fill in the JTable 
	 * @throws SQLException
	 */
	public static DefaultTableModel buildTableModel() throws SQLException {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement viewStatement = null;
		ResultSet rs = null;
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		Vector<String> columnNames = null;
		try {
			viewStatement = connection.prepareStatement(viewStatementString);
			rs = viewStatement.executeQuery();
			ResultSetMetaData metaData = rs.getMetaData();
			columnNames = new Vector<String>();
		    int columnCount = metaData.getColumnCount();
		    for (int column = 1; column <= columnCount; column++) {
		        columnNames.add(metaData.getColumnName(column));
		    }

		    // data of the table
		    while (rs.next()) {
		        Vector<Object> vector = new Vector<Object>();
		        for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
		            vector.add(rs.getObject(columnIndex));
		        }
		        data.add(vector);
		    }
		} catch(Exception e){
			
		}
	    // names of columns
	    
	    return new DefaultTableModel(data, columnNames);

	}
	
	public static void fillComboBox(){
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement viewStatement = null;
		ResultSet rs = null;
		try {
			viewStatement = connection.prepareStatement(viewStatementString);
			rs = viewStatement.executeQuery();
			while(rs.next()){
				Controller.productbox.addItem(rs.getString(2));
			}
		}catch(SQLException e){
		}finally {
			ConnectionFactory.close(viewStatement);
			ConnectionFactory.close(connection);
		}
	}
	
	/**
	 * Deletes the product with id given.
	 * @param id the id by which we select which row to delete
	 */
	public static void delete(int id){
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = connection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1, id);
			System.out.println(deleteStatement);
			deleteStatement.execute();
			
		}catch(SQLException e){
			LOGGER.log(Level.WARNING, "ClientDAO:delete " + e.getMessage());
		}finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(connection);
		}
	}
	
	/**
	 * Updates the fields in a specific row.
	 * @param id the index in which the field must be updated
	 * @param p product object with whose data the field must be updated
	 */
	public static void update(int id, Product p){
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		try {
			updateStatement = connection.prepareStatement(updateStatementString);
			updateStatement.setString(1, p.getName());
			updateStatement.setInt(2, p.getAmount());
			updateStatement.setDouble(3, p.getPrice());
			updateStatement.setInt(4, id);
			System.out.println(updateStatement);
			updateStatement.execute();
			
		}catch(SQLException e){
			LOGGER.log(Level.WARNING, "ClientDAO:delete " + e.getMessage());
		}finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(connection);
		}
	}
}
