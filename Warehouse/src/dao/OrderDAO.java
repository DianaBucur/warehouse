package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Order;
import model.Product;

public class OrderDAO {
	
	protected static final Logger LOGGER = Logger.getLogger(OrderDAO.class.getName());
	
	private static final String insertStatementString = "INSERT INTO orders (cname,pname,amount)" + " VALUES (?,?,?)";
	private static final String selectStatementString = "SELECT * FROM orders ORDER BY id DESC LIMIT 1";
	
	/**
	 * Insert an order the 'order' table from database.
	 * @param o the order to be added in the database
	 * @return the index in which it was inserted
	 */
	public static int insert(Order o) {
		Connection connection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = connection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, o.getCname());
			insertStatement.setString(2, o.getPname());
			insertStatement.setInt(3, o.getAmount());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(connection);
		}
		return insertedId;
	}
	
	/**
	 * Selects the last row of the 'order' table from database.
	 * @return the last order submitted
	 */
	public static Order findLastOrder() {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement selectStatement = null;
		ResultSet rs = null; Order o = null;
		try {
			selectStatement = connection.prepareStatement(selectStatementString);
			rs = selectStatement.executeQuery();
			rs.next();

			int id = rs.getInt(1);
			String cname = rs.getString(2);
			String pname = rs.getString(3);
			int amount = rs.getInt(4);
			o = new Order(id, cname, pname, amount);
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(selectStatement);
			ConnectionFactory.close(connection);
		}
		return o;
	}
}
