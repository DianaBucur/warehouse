package presentation;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.log.SysoLogger;
import com.itextpdf.text.pdf.FontSelector;
import com.itextpdf.text.pdf.PdfWriter;

import dao.ClientDAO;
import dao.ProductDAO;
import model.Client;
import model.Order;
import model.Product;

public class BillPDF {
	/**
	 * Generates the pdf file for an order.
	 * @param o the order that needes to have the pdf created
	 */
	public BillPDF(Order o){
		Document document = new Document();
		String filename = "Order" + (o.getId() + 1) + ".pdf";
		
		try{
		PdfWriter.getInstance(document, new FileOutputStream(filename));
		document.open();
		document.add(new Phrase("\n"));
		document.add(new Phrase("\n"));
		document.add(new Phrase("\n"));
		document.add(new Phrase("\n"));
		document.add(new Phrase("\n"));
		document.add(new Phrase("\n"));
		document.add(new Phrase("\n"));
		
		Font f1 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 45);
		FontSelector selector = new FontSelector();
		selector.addFont(f1);
		Phrase title = selector.process('\n' + "Order Bill");
		Paragraph title2 = new Paragraph();
		title2.add(title);
		title2.setAlignment(Element.ALIGN_CENTER);
		document.add(title2);
		document.add(new Phrase("\n"));
		document.add(new Phrase("\n"));
		
		
		Font f2 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 18);
		FontSelector selector2 = new FontSelector();
		selector2.addFont(f2);
		Phrase details = selector2.process("Transation details: ");
		Paragraph title3 = new Paragraph();
		title3.add(details);
		document.add(title3);
		document.add(new Phrase("\n"));
		
		Paragraph para = new Paragraph("Order no: #" + (o.getId()+1));
		document.add(para);
		String timeStamp = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(Calendar.getInstance().getTime());
		Paragraph odate = new Paragraph("Submission date: " + timeStamp);
		document.add(odate);
		Client c = ClientDAO.findByName(o.getCname());
		Paragraph para2 = new Paragraph("Full name: " + c.getName());
		document.add(para2);
		Paragraph para3 = new Paragraph("Address: " + c.getAddress());
		document.add(para3);
		Paragraph para4 = new Paragraph("Email Address: " + c.getEmail());
		document.add(para4);
		document.add(new Phrase("\n"));
		
		
		document.add(new Phrase("\n"));
		Phrase ordered = selector2.process("Ordered products: ");
		Paragraph title4 = new Paragraph();
		title4.add(ordered);
		document.add(title4);
		document.add(new Phrase("\n"));
		
		Paragraph para5 = new Paragraph(o.getPname() + "          x" + o.getAmount());
		document.add(para5);
		Product p = ProductDAO.findByName(o.getPname());
		Paragraph para6 = new Paragraph("Total: " + o.getAmount()*p.getPrice());
		para6.setAlignment(Element.ALIGN_RIGHT);
		document.add(para6);
		document.add(new Phrase("\n"));
		Paragraph exp = new Paragraph("Order expected to arrive in about 3-7 days from the date of submission. Please contact us in case of any inconvenience at the following email addres: warehouse-tennis@contact.com. Thank you for choosing the best tennis equipment provider !");
		exp.setFirstLineIndent(50);
		document.add(exp);
		
		}catch(Exception e){
			
		}
		document.close();
	}
}
