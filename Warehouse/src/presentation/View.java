package presentation;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class View extends JFrame{

	private JButton clients;
	private JButton products;
	private JButton orders;
	private JPanel contentPane = new JPanel();
	
	/**
	 * The components of the main frame. 
	 */
	public View(){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 681, 459);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		clients = new JButton("Manage Clients");
		clients.setBounds(15, 172, 158, 51);
		panel.add(clients);
		
		products = new JButton("Manage Products");
		products.setBounds(233, 172, 167, 51);
		panel.add(products);
		
		orders = new JButton("Make Order");
		orders.setBounds(453, 172, 167, 51);
		panel.add(orders);
	}

	public void addClientsListener(ActionListener a){
		clients.addActionListener(a);
	}
	
	public void addProductsListener(ActionListener a){
		products.addActionListener(a);
	}
	
	public void addOrderListener(ActionListener a){
		orders.addActionListener(a);
	}
}
