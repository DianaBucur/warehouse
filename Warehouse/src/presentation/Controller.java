package presentation;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import bll.ClientBLL;
import bll.ProductBLL;
import connection.ConnectionFactory;
import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import model.Client;
import model.Order;
import model.Product;
import net.proteanit.sql.DbUtils;

public class Controller {
	private View view;
	public static JComboBox clientbox;
	public static JComboBox productbox;
	
	/**
	 * Adds the action listeners for the component created in the view.
	 * @param view
	 */
	public Controller(View view){
		this.view = view;
		view.addClientsListener(new actionClients());
		view.addProductsListener(new actionProducts());
		view.addOrderListener(new actionOrder());
	}
	
	class actionClients implements ActionListener{
		public void actionPerformed(ActionEvent a) {
				JFrame frame = new JFrame();
				frame.setBounds(100, 100, 815, 554);
				JPanel contentPane = new JPanel();
				contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
				contentPane.setLayout(new BorderLayout(0, 0));
				frame.setContentPane(contentPane);
					
					JPanel panel = new JPanel();
					contentPane.add(panel, BorderLayout.CENTER);
					panel.setLayout(null);
					
					JScrollPane table = new JScrollPane();
					table.setBounds(15, 62, 448, 290);
					panel.add(table);
					
					JLabel namestr = new JLabel("Name:");
					namestr.setBounds(487, 73, 69, 20);
					panel.add(namestr);
					
					JTextField name = new JTextField();
					name.setBounds(564, 70, 204, 26);
					panel.add(name);
					name.setColumns(10);
					
					JLabel addressstr = new JLabel("Address:");
					addressstr.setBounds(487, 124, 69, 20);
					panel.add(addressstr);
					
					JTextField address = new JTextField();
					address.setBounds(564, 121, 204, 26);
					panel.add(address);
					address.setColumns(10);
					
					JLabel emailstr = new JLabel("Email:");
					emailstr.setBounds(487, 175, 69, 20);
					panel.add(emailstr);
					
					JTextField email = new JTextField();
					email.setBounds(564, 172, 204, 26);
					panel.add(email);
					email.setColumns(10);
					
					JButton insert = new JButton("Insert Client");
					insert.addActionListener(new ActionListener(){
						public void actionPerformed(ActionEvent arg0) {
							String cname = name.getText();
							String caddress = address.getText();
							String cemail = email.getText();
							Client client = new Client(cname, caddress, cemail);
							ClientBLL cbll = new ClientBLL();
							/*if(cbll.insertClient(client) < 0){
								JOptionPane.showMessageDialog(frame, "Please enter a valid email address!");
							}
							else{*/
								ClientDAO.insert(client);
								JOptionPane.showMessageDialog(frame, "Client inserted successfully!");
							//}
						}
					});
					insert.setBounds(554, 241, 162, 29);
					panel.add(insert);
					
					JButton viewc = new JButton("View All Clients");
					viewc.addActionListener(new ActionListener(){
						public void actionPerformed(ActionEvent arg0) {
							JTable table2;
							try {
								table2 = new JTable(ClientDAO.buildTableModel());
								JScrollPane scrollpane = new JScrollPane(table2);
								scrollpane.setBounds(15, 62, 448, 290);
								panel.add(scrollpane);
								
							} catch (SQLException e) {
								e.printStackTrace();
							}
							
						}
					});
					viewc.setBounds(148, 401, 162, 29);
					panel.add(viewc);
					
					clientbox = new JComboBox();
					ClientDAO.fillComboBox();
					clientbox.addItemListener(new ItemListener(){
						public void itemStateChanged(ItemEvent e) {
							String name2 = (String)clientbox.getSelectedItem();
							Client client = ClientDAO.findByName(name2);
							name.setText(client.getName());
							address.setText(client.getAddress());
							email.setText(client.getEmail());
							
						}
					});
					clientbox.setBounds(528, 16, 220, 26);
					panel.add(clientbox);
					
					JButton delete = new JButton("Delete Client");
					delete.addActionListener(new ActionListener(){
						public void actionPerformed(ActionEvent arg0) {
							String name = (String)clientbox.getSelectedItem();
							Client client = ClientDAO.findByName(name);
							ClientDAO.delete(client.getId());
						}
					});
					delete.setBounds(554, 401, 162, 29);
					panel.add(delete);
					
					JButton update = new JButton("Update Client");
					update.addActionListener(new ActionListener(){
						public void actionPerformed(ActionEvent arg0) {
							String cname = name.getText();
							String caddress = address.getText();
							String cemail = email.getText();
							Client client = new Client(cname, caddress, cemail);
							String name = (String)clientbox.getSelectedItem();
							Client client2 = ClientDAO.findByName(name);
							ClientBLL cbll = new ClientBLL();
							//if(cbll.insertClient(client) < 0){
							//	JOptionPane.showMessageDialog(frame, "Please enter a valid email address!");
							//	ClientDAO.update(client2.getId(), client);
							//}
							//else{
								ClientDAO.update(client2.getId(), client);
								JOptionPane.showMessageDialog(frame, "Client updated successfully!");
							//}
						}
					});
					update.setBounds(554, 323, 162, 29);
					panel.add(update);
			
			frame.setVisible(true);
		}
	}
	
	class actionProducts implements ActionListener{
		public void actionPerformed(ActionEvent a) {
			JFrame frame = new JFrame();
			frame.setBounds(100, 100, 815, 554);
			JPanel contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			contentPane.setLayout(new BorderLayout(0, 0));
			frame.setContentPane(contentPane);
			
			JPanel panel = new JPanel();
			contentPane.add(panel, BorderLayout.CENTER);
			panel.setLayout(null);
			
			JScrollPane table = new JScrollPane();
			table.setBounds(15, 62, 448, 290);
			panel.add(table);
			
			JLabel namestr = new JLabel("Name:");
			namestr.setBounds(487, 73, 69, 20);
			panel.add(namestr);
			
			JTextField name = new JTextField();
			name.setBounds(564, 70, 204, 26);
			panel.add(name);
			name.setColumns(10);
			
			JLabel amountstr = new JLabel("Amount:");
			amountstr.setBounds(487, 124, 69, 20);
			panel.add(amountstr);
			
			JTextField amount = new JTextField();
			amount.setBounds(564, 121, 204, 26);
			panel.add(amount);
			amount.setColumns(10);
			
			JLabel pricestr = new JLabel("Price:");
			pricestr.setBounds(487, 175, 69, 20);
			panel.add(pricestr);
			
			JTextField price = new JTextField();
			price.setBounds(564, 172, 204, 26);
			panel.add(price);
			price.setColumns(10);
			
			JButton insert = new JButton("Insert Product");
			insert.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent arg0) {
					String pname = name.getText();
					int pamount = Integer.parseInt(amount.getText());
					double pprice = Double.parseDouble(price.getText());
					Product p = new Product(pname, pamount, pprice);
					ProductBLL pbll = new ProductBLL();
					if(pbll.insertProduct(p) < 0){
						JOptionPane.showMessageDialog(frame, "The amount can't be negative ! Please enter a valid number.");
					}
					else{
						ProductDAO.insert(p);
						JOptionPane.showMessageDialog(frame, "Product inserted successfully!");
					}
				}
			});
			insert.setBounds(554, 241, 162, 29);
			panel.add(insert);
			
			JButton viewp = new JButton("View All Products");
			viewp.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent arg0) {
					JTable table2;
					try {
						table2 = new JTable(ProductDAO.buildTableModel());
						JScrollPane scrollpane = new JScrollPane(table2);
						scrollpane.setBounds(15, 62, 448, 290);
						panel.add(scrollpane);
						
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			});
			viewp.setBounds(148, 401, 162, 29);
			panel.add(viewp);
			
			productbox = new JComboBox();
			ProductDAO.fillComboBox();
			productbox.addItemListener(new ItemListener(){
				public void itemStateChanged(ItemEvent e) {
					String name2 = (String)productbox.getSelectedItem();
					Product p = ProductDAO.findByName(name2);
					name.setText(p.getName());
					amount.setText(String.valueOf(p.getAmount()));
					price.setText(String.valueOf(p.getPrice()));
					
				}
			});
			productbox.setBounds(528, 16, 220, 26);
			panel.add(productbox);
			
			JButton delete = new JButton("Delete Product");
			delete.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent arg0) {
					String name = (String)productbox.getSelectedItem();
					Product p = ProductDAO.findByName(name);
					ProductDAO.delete(p.getId());
				}
			});
			delete.setBounds(554, 401, 162, 29);
			panel.add(delete);
			
			JButton update = new JButton("Update Product");
			update.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent arg0) {
					String pname = name.getText();
					int pamount = Integer.parseInt(amount.getText());
					double pprice = Double.parseDouble(price.getText());
					Product p = new Product(pname, pamount, pprice);
					String name = (String)productbox.getSelectedItem();
					Product p2 = ProductDAO.findByName(name);
					ProductBLL pbll = new ProductBLL();
					if(pbll.insertProduct(p) < 0){
						JOptionPane.showMessageDialog(frame, "The amount can't be a negative number!");
					}
					else {
						ProductDAO.update(p2.getId(), p);
						JOptionPane.showMessageDialog(frame, "The product was updated successfully!");
					}
						
				}
			});
			update.setBounds(554, 323, 162, 29);
			panel.add(update);
			frame.setVisible(true);
		}
	}
	
	class actionOrder implements ActionListener{
		public void actionPerformed(ActionEvent a) {
			JFrame frame = new JFrame();
			frame.setBounds(100, 100, 790, 509);
			JPanel contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			contentPane.setLayout(new BorderLayout(0, 0));
			frame.setContentPane(contentPane);
			
			JPanel panel = new JPanel();
			contentPane.add(panel, BorderLayout.CENTER);
			panel.setLayout(null);
			
			JLabel amountstr = new JLabel("Amount:");
			amountstr.setBounds(268, 203, 69, 20);
			panel.add(amountstr);
			
			JTextField amount = new JTextField();
			amount.setBounds(354, 200, 204, 26);
			panel.add(amount);
			amount.setColumns(10);
			
			productbox = new JComboBox();
			ProductDAO.fillComboBox();
			productbox.setBounds(413, 99, 271, 26);
			panel.add(productbox);
			
			clientbox = new JComboBox();
			ClientDAO.fillComboBox();
			clientbox.setBounds(76, 99, 271, 26);
			panel.add(clientbox);
			
			JButton sorder = new JButton("Submit Order");
			sorder.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					String cname = (String) clientbox.getSelectedItem();
					String pname = (String) productbox.getSelectedItem();
					int amount2 = Integer.parseInt(amount.getText());
					Product p = ProductDAO.findByName(pname);
					Product p2 = new Product(p.getName(), p.getAmount() - amount2, p.getPrice());
					ProductBLL pbll = new ProductBLL();
					if(pbll.insertProduct(p2) < 0 || amount2 < 0)
						JOptionPane.showMessageDialog(frame, "There are not enough products on stock for this order! Please enter a valid number.");
					else{
						Order o = new Order(cname, pname, amount2);
						OrderDAO.insert(o);
						JOptionPane.showMessageDialog(frame, "Your order has been submitted successfully.");
						ProductDAO.update(p.getId(), p2);
						Order o2 = OrderDAO.findLastOrder();
						BillPDF bill = new BillPDF(o2);
					}
				}
			});
			sorder.setBounds(306, 307, 173, 51);
			panel.add(sorder);
			
			JLabel lblSelectClient = new JLabel("Select Client:");
			lblSelectClient.setBounds(167, 63, 117, 20);
			panel.add(lblSelectClient);
			
			JLabel lblSelectProduct = new JLabel("Select Product:");
			lblSelectProduct.setBounds(495, 63, 117, 20);
			panel.add(lblSelectProduct);
			frame.setVisible(true);
		}
	}
	
}
