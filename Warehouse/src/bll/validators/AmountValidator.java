package bll.validators;

import javax.swing.JOptionPane;

import model.Product;

public class AmountValidator implements Validator<Product> {

	/**
	 * Check if the amount is greater than 0 and return -1 otherwise.
	 */
	public int validate(Product p){
		if(p.getAmount() < 0){
			return -1;
		}
		else
			return 0;
	}
}
