package bll;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.EmailValidator;
import bll.validators.Validator;
import dao.ClientDAO;
import model.Client;

public class ClientBLL{
	
	private List<Validator<Client>> validators;
	
	/**
	 * Contructor of the business layer for client.
	 */
	public ClientBLL(){
		validators = new ArrayList<Validator<Client>>();
		validators.add(new EmailValidator());
	}
	
	/**
	 * Check if the name is found and sets error if not.
	 * @param name name to be searched for in the database.
	 * @return the object found
	 */
	public Client findClientByName(String name) {
		Client c = ClientDAO.findByName(name);
		if (c == null) {
			throw new NoSuchElementException("The client with name =" + name + " was not found!");
		}
		return c;
	}

	/**
	 * Checks before inserting a client if all its fields are valid.
	 * @param client client to be checked if valid
	 * @return
	 */
	public int insertClient(Client client) {
		for (Validator<Client> v : validators) {
			v.validate(client);
		}
		return ClientDAO.insert(client);
	}
	
}

