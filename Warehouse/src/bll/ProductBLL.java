package bll;

import java.util.ArrayList;
import java.util.List;

import bll.validators.AmountValidator;
import bll.validators.Validator;
import dao.ProductDAO;
import model.Product;

public class ProductBLL {
	
private List<Validator<Product>> validators;
	
	public ProductBLL(){
		validators = new ArrayList<Validator<Product>>();
		validators.add(new AmountValidator());
	}

	/**
	 * Checks before inserting a product if all its fields are valid.
	 * @param p product to be checked if valid
	 * @return
	 */
	public int insertProduct(Product p) {
		for (Validator<Product> v : validators) {
			if(v.validate(p) < 0)
				return -1;
		}
		return 0;
	}
}
